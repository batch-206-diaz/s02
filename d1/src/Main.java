import java.util.*;

public class Main {
    public static void main(String[] args) {
    //System.out.println("Hello world!");

/*
    Operators in Java
        - Arithmetic (+ - * /), Comparison (>,<,<=,>=,==,!=), Logical (&&,||,!), Assignment (=)
*/

        int num1 = 15;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        num1 = 36;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        Scanner numberScanner = new Scanner(System.in);

        System.out.println("Enter a number");
        int number = numberScanner.nextInt();

        if(number % 2 == 0){
            System.out.println(number + " is even!");
        }else{
            System.out.println(number + " is odd!");
        }

        /*
            Switch Cases
                - Switch statements are control flow structures
                - Allow a specific block to be run out of many other code blocks by comparing the given value against each case/condition provided
                - Matching the condition runs that code block
                - Mostly used if user input is predictable
                - Default case is added to run in the event of "no match found"


            Arrays
                - Objects that can contain data
                - In Java, they have a fixed/limited number of values with the same data type
                - Unlike JS, the length of Java arrays are established when the array is created
                - Cannot use direct sout, need to convert array into string to display values
                - Syntax:
                    dataType[] identifier = new dataType[numberOfElements];
                    dataType[] identifier = {elementA,elementB,...};
                - Methods:
                    Arrays.toString() - used to convert array to string
                    Arrays.sort() -
                    Arrays.binarySearch() - allows to pass an argument/item, returns index

            ArrayList
                - are resizable collections/arrays that function similarly to arrays in JS
                - using the new keyword does not require the datatype for the array list
                - Can be directly printed with sout, does not need to be converted
                - Syntax:
                    ArrayList<dataType> identifier = new Arraylist<>();
                    identifier.add(elementA);
                - Methods:
                    - get() - arrayListName.get(index), retrieves item by its index
                    - set() - arrayListName.set(index), update an item by its index
                    - add() - arrayListName.add(value), adds an item by its index
                    - remove() - arrayListName.remove(index), removes an item by its index
                    - clear() - arrayListName.clear(), clears out all items in the array list
                    - size() - arrayListName.size(), gets the length of the array list

            HashMaps
                - Most objects in java are pre-defined and are instantiations of existing Classes that contain a proper set of properties and methods. It wouldn't be appropriate to use if the goal is to only store a collection of data in key-value pairs.
                - HashMaps offer flexibility
                - "keys" are also referred as "fields"
                - Syntax:
                    HashMap<fieldDataType,valueDataType> identifier = new HashMap<>();
                - Methods:
                    - put() - hashMapName.put(field,value)
                    - get() - hashMapName.get("field"), retrieves values by their fields
                    - remove() - hashMapName.remove("field"), remove element/field-value
                    - keySet() - hashMapName.keySet(), retrieves hashmap keys
        */

/* SWITCH */

        System.out.println("Enter a number from 1-4 to see ont of the four main SM malls");
        int directionValue = numberScanner.nextInt();

        switch(directionValue){

            case 1:
                System.out.println("SM North Edsa");
                break;
            case 2:
                System.out.println("SM Southmall");
                break;
            case 3:
                System.out.println("SM City Taytay");
                break;
            case 4:
                System.out.println("SM Manila");
                break;
            default:
                System.out.println("Invalid.");
        }

/* ARRAYS */

        String[] newArr = new String[3];
        newArr[0] = "Clark";
        newArr[1] = "Bruce";
        newArr[2] = "Diana";
        //newArr[3] = "Barry"; -- out of bounds, only three values
        //newArr[2] = 25; -- cannot use other data type

        /* WITH INITIALIZED VALUES */
        String[] arrSample = {"Tony","Thor","Steve"};
        System.out.println(Arrays.toString(arrSample));
        // arrSample[3] = "Peter"; -- out of bounds

        //System.out.println(newArr); -- cannot show //result: [Ljava.lang.String;@482f8f11
        System.out.println(Arrays.toString(newArr));

        /*SORT METHOD*/
        Arrays.sort(newArr);
        System.out.println("Result of the Arrays.sort()");
        System.out.println(Arrays.toString(newArr));

        Integer[] intArr = new Integer[3];
        intArr[0] = 54;
        intArr[1] = 12;
        intArr[2] = 67;
        System.out.println("Initial Order of the intArr:");
        System.out.println(Arrays.toString(intArr));
        System.out.println("Sorted Order of the intArr:");
        Arrays.sort(intArr, Collections.reverseOrder()); // sort in descending
        System.out.println(Arrays.toString(intArr));


        /* BINARY SEARCH METHOD */
        String searchTerm = "Bruce";
        int result = Arrays.binarySearch(newArr,searchTerm);
        System.out.println("The index of " + searchTerm + " is: " + result);

/* ARRAY LIST */

        ArrayList<String> students = new ArrayList<>();

        students.add("Paul");
        students.add("John");
        System.out.println(students);

        /* WITH INITIALIZED VALUES */
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill Gates","Elon Musk","Jeff Bezos"));
        System.out.println(employees);
        employees.add("Lucio Tan");
        System.out.println(employees);

        /* GET METHOD */
        System.out.println(students.get(1));

        /* SET METHOD */
        students.set(0,"George");
        System.out.println(students);

        /* ADD METHOD */
        students.add("Ringo");
        System.out.println(students);

        /* REMOVE METHOD */
        students.remove(1);
        System.out.println(students);

        /* CLEAR METHOD */
        students.clear();
        System.out.println(students);

        /* SIZE METHOD */
        System.out.println(students.size());

/* HASH MAP */

        HashMap<String,String> userRoles = new HashMap<>();

        userRoles.put("Anna","Admin");
        userRoles.put("Alice","User");
        System.out.println(userRoles);

        userRoles.put("Alice","Teacher"); // will update existing instead of adding
        System.out.println(userRoles);

        userRoles.put("Dennis","User"); // will add new key-value pair
        System.out.println(userRoles);


        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Dennis"));
        System.out.println(userRoles.get("Anna"));
        System.out.println(userRoles.get("alice"));
        System.out.println(userRoles.get("Ellen"));

        userRoles.remove("Dennis");
        System.out.println(userRoles);

        System.out.println(userRoles.keySet());
    }
}