import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");

        Scanner fruitScanner = new Scanner(System.in);

/* ARRAY */

        String[] fruitStock = {"apple","avocado","banana","kiwi","orange"};
        System.out.println("Fruits in stock: " + Arrays.toString(fruitStock));
        System.out.println("Which fruit would you like to get the index of?");
        String fruit = fruitScanner.nextLine();
        int index = Arrays.binarySearch(fruitStock,fruit);
        System.out.println("The index of " + fruit + " is: "+ index);

/* ARRAY LIST */

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("Debug","Hana","Annie","Party"));
        System.out.println("My friends are: " + friends);

/* HASH MAP */

        HashMap<String,Integer> inventory = new HashMap<>();
        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);

        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);
    }
}